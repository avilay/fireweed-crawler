import subprocess
from flask import Flask, jsonify

app = Flask(__name__)


def is_authorized():
    gae_cron = False
    if 'X-Appengine-Cron' in request.headers:
        gae_cron = request.headers['X-Appengine-cron'] == 'true'
    return gae_cron


@app.route('/crawl')
def crawl():
    if not is_authorized():
        abort(401)
    subprocess.run(['python3', '-m', 'crawler.main'])
    return jsonify({'message': 'completed'})


@app.route('/wirecheck')
def wirecheck():
    return jsonify({
        'message': 'wirecheck complete'
    })


@app.errorhandler(401)
def handle_forbidden(e):
    err = {
        'message': 'You are not authorized to run the crawl job!'
    }
    return jsonify(err)
