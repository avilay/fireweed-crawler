# Has to be a google-appengine base if we want to run cron
FROM gcr.io/google-appengine/python

ENV APP_ROOT /fireweed
WORKDIR ${APP_ROOT}

ADD crawler ${APP_ROOT}/crawler
ADD app.py ${APP_ROOT}
ADD requirements.txt ${APP_ROOT}
ENV ENV=prod

# Install required python packages
RUN pip3 install -r requirements.txt

# GAE expects the endpoint to listen on port 8080
CMD gunicorn -b :8080 app:app
