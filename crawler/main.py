import os
import requests
import logging

from crawler.parser import InvalidFeed, InvalidPodcast, InvalidUrl, parse
from .models.pg_datastore import PgDatastore

METADATA_URL = 'http://metadata/computeMetadata/v1/project/attributes/podcasts_config'
logger = logging.getLogger(__package__)


def read_config():
    env = os.environ.get('ENV', 'dev')
    logger.info(f'Operating in {env} env')
    if env == 'prod':
        resp = requests.get(METADATA_URL, headers={'Metadata-Flavor': 'Google'})
        return resp.json()
    else:
        return {
            'POSTGRES': {
                'host': 'localhost',
                'user': 'postgres',
                'database': 'fireweed'
            }
        }


def config_log():
    logformat = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
    formatter = logging.Formatter(logformat)

    ch = logging.StreamHandler()
    ch.setFormatter(formatter)

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)
    root_logger.addHandler(ch)


def crawl(ds):
    podcasts = ds.get_all_podcasts()
    num_new_episodes = 0
    for podcast in podcasts:
        logger.info(f'Crawling id {podcast.podcast_id} {podcast.feed_url}')
        try:
            pc, eps = parse(podcast.feed_url)
            pc.podcast_id = podcast.podcast_id
            ds.update_podcast(pc)
            logger.info(f'Found {len(eps)} episodes in {podcast.podcast_id}')
            for ep in eps:
                if ds.get_episode_by_guid(ep.guid) is None:
                    logger.debug(f'Found new episode {ep.guid} for {podcast.podcast_id}')
                    num_new_episodes += 1
                    ep.podcast_id = podcast.podcast_id
                    ds.add_episode(ep)
        except InvalidPodcast as ip:
            logger.warn(f'Feed {podcast.feed_url} does not have any podcasts!')
        except InvalidFeed as ifd:
            logger.warn(f'{podcast.feed_url} is not a valid RSS/ATOM feed!')
        except InvalidUrl as iu:
            logger.warn(f'{podcast.feed_url} is not a valid url!')
    ds.add_crawl_history(len(podcasts), num_new_episodes)


if __name__ == '__main__':
    config = read_config()
    config_log()
    ds = PgDatastore(config)
    crawl(ds)
