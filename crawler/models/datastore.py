from abc import ABC, abstractmethod

from .episode import Episode
from .podcast import Podcast


class EntityNotFoundError(Exception):
    pass


class Datastore(ABC):
    @abstractmethod
    def add_podcast(self, podcast: Podcast):
        pass

    @abstractmethod
    def update_podcast(self, podcast: Podcast):
        pass

    @abstractmethod
    def add_episode(self, episode: Episode):
        pass

    @abstractmethod
    def get_podcast(self, podcast_id):
        pass

    @abstractmethod
    def get_podcast_by_feed_url(self, feed_url):
        pass

    @abstractmethod
    def get_all_podcasts(self):
        pass

    @abstractmethod
    def get_episodes(self, podcast_id):
        pass

    @abstractmethod
    def get_episode(self, podcast_id, episode_id):
        pass

    @abstractmethod
    def get_episode_by_guid(self, guid):
        pass
