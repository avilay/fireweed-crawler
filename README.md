Fireweed Services
=================
First questions first - why this name? I like to name my projects after plants found in the Pacific Northwest. This is one of them, also known as [Chamaenerion angustifolium](https://en.wikipedia.org/wiki/Chamaenerion_angustifolium)

Firweed group of services is a stack of demo micro-services. Each micro-service will have its own git repo called fireweed-{service name}.

The full fireweed app is a podcast playlist manager, where end users can add episodes from across different podcasts into their playlist.


Fireweed Crawler
================
This project demonstrates how to wrap your existing Docker based batch process as a GAE cron job. At a high level, this micro-service is a worker service that crawls for new episodes for all podcasts existting in the system. It consists of two components - the PostgreSQL database where podcasts are stored, and the crawler program which will do the actual crawling. Because GAE cron works on URLs on a WSGI app, there is an additional light-weight Flask app that calls the crawler process whenever the `/crawl` URL is invoked by GAE. Only the GAE cron is allowed to call this URL. For any other caller it will return a 404 Forbidden status.


Deployment
==========
### Deploy Fireweed Podcasts
Deploy the Fireweed Podcast service described [here](https://bitbucket.org/avilay/fireweed-podcasts/src/master/). Do not deploy the optional cron service from there.

### Setup PROEJCT-ID
Grab your SQL instance connection name from GCP and replace {YOUR-GCP-POSTGRES-INSTANCE-CONN-NAME}.

### Deploy App
Deploy the Flask app as the `crawler` service.
```
$ cd fireweed-crawler
$ gcloud app deploy
```

### Deploy cron
```
$ cd fireweed-crawler
$ gcloud app deploy cron.yaml
```
